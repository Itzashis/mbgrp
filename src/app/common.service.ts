import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }

  /******************
   * Getting images
   */

  public getGallery(): Observable<any> {
    // return this.http.get('http://localhost/app/view_img.php');
    return this.http.get('http://localhost:4000/getimages');
  }
}
