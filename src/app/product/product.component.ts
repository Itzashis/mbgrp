import { Component, OnInit } from '@angular/core';
import {CommonService} from "../common.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  images: any;
  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.commonService.getGallery().subscribe((image) => {
      this.images = image;
      console.log('images', this.images);
    });
  }

}
