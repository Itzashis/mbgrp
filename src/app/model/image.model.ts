export interface Image {
    url: string;
    rows: string;
    col: string;
}
