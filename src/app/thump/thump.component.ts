import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-thump',
  templateUrl: './thump.component.html',
  styleUrls: ['./thump.component.scss']
})
export class ThumpComponent implements OnInit {
  @Input() srcUrl: any;
  constructor() { }

  ngOnInit(): void {
  }

}
