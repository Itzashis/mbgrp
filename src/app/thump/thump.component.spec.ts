import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThumpComponent } from './thump.component';

describe('ThumpComponent', () => {
  let component: ThumpComponent;
  let fixture: ComponentFixture<ThumpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThumpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThumpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
