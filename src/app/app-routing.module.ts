import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { OfficeGalleryComponent } from './office-gallery/office-gallery.component';
import {ProductComponent} from "./product/product.component";

// const routes: Routes = [];
const routes: Routes = [
  { path: 'home', component: HomeComponent },
  // { path: 'gallery', component: GalleryComponent },
  { path: 'gallery', component: OfficeGalleryComponent },
  // { path: 'about-us', component: AboutUsComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'product', component: ProductComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
